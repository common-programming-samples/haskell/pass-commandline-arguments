import System.Environment(getArgs)

main = do
    args <- getArgs
    if (args == [])
        then putStrLn "no args passed"
        else print args
