# pass-commandline-arguments

Basically all you have to do to parse command line arguments is to call the function `getArgs` of the `System.Environment`. It is
of type `IO [String]` so ideally call it in your main and extract the args into a `[String]` before you continue processing them.

The actual code sample is this:

```Haskell
import System.Environment(getArgs)

main = do
    args <- getArgs
    if (args == [])
        then putStrLn "no args passed"
        else print args
```

To call this code, just call
```stack run <your arguments>```

Or alternatively just use the Dockerfile contained in this repo
